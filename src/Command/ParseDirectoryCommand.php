<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 14/07/18
 * Time: 16:49
 */

namespace App\Command;

use App\Entity\Directory;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand as Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseDirectoryCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:directory:parse')
            ->setDescription('get all new directories')
            ->addArgument('path')
            ->addArgument('date');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dtime = date_create_from_format("Y-m-d", $input->getArgument('date'));
        $date = $dtime->getTimestamp();

        $output->writeln([
            'Reading directory',
            '============',
            $date,
        ]);
//        $fileSystem = new Filesystem();
//        dump($input->getArgument('path'));
        $path = $input->getArgument('path', true);

        // get entity manager
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $i = 0;
        if ($foler = opendir($path)) {
            $output->writeln(("ouverture du répertoire" . $path));
            while (false !== ($fichier = readdir($foler))) {
                if ($fichier == '.' or $fichier == '..') continue;
                $subPath = $path . $fichier;
                if (is_dir($subPath)) {
                    $output->writeln($fichier);

                    if (!$found = $em->getRepository('App\Entity\Directory')->findOneBy(['name' => $fichier])) {
                        $dir = new Directory();
                        $dir->setName($fichier);
                        $now = new \DateTime();
                        $dir->setCreateTime(new \DateTime());
                        $now->setTimestamp(filectime($subPath));
                        $dir->setDirectoryTime($now);
                        try {
                            $artist = $this->getArtist( $this->recurseRead($subPath));
                        } catch (\Exception $e) {
                            $artist = $subPath;
                        }
                        $dir->setArtist($artist);
                        $em->persist($dir);
                        $i++;
                    } else {
                        $output->writeln($fichier . 'already exists in database');
                    }
                }
            }
        }

        $em->flush();
        $output->writeln($i . 'saved entries');
    }

    /**
     * return data about folder
     *
     * @param $path
     * @return mixed
     * @throws \Exception
     */
    private function getArtist($path)
    {
        if ($foler = opendir($path)) {
            while (false !== ($fichier = readdir($foler))) {
                if ($fichier == '.' or $fichier == '..') continue;
                $subPath = $path . '/' . $fichier;
                if (is_dir($subPath)) {
                    $this->getArtist($subPath);
                } else {
                    try {
                        $getid3 = new \getID3();
                        $t = $getid3->analyze($subPath);
                        $tags = $t['id3v2']['comments'];
//                    var_dump(reset($tags['year']));
//                    var_dump(reset($tags['artist']));
//                    var_dump(reset($tags['album']));
//                    var_dump(reset($tags['title']));
                        return reset($tags['artist']);
                    } catch (\Exception $e) {
                        throw new \Exception('error durrin get Metadata', 500);
                    }
                }
            }
        }
    }

    private function recurseRead($path)
    {
        if ($foler = opendir($path)) {
            while (false !== ($fichier = readdir($foler))) {
                if ($fichier == '.' or $fichier == '..') continue;
                var_dump("ouverture du répertoire" . $path);
                $subPath = $path . '/' . $fichier;
                if (is_dir($subPath)) {cal
                    $files = glob($subPath . '/*.mp3');

                    if ($files !== false && count($files) > 0) {
                        $filecount = count($files);
                       return $this->getArtist($subPath);
                    } else {
                        $this->recurseRead($subPath);
                    }
                }
            }
        }
    }
}